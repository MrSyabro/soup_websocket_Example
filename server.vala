/* server.vala
 *
 * Copyright 2020 MrSyabro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//static Soup.WebsocketConnection ws;

// Тут все по канонам доков (почти)
public class WSServer : Soup.Server {
    private int access_counter = 0;
    private Soup.WebsocketConnection ws;
    public WSServer(){
        assert (this != null);
        string[] protocols = {"chat"};
        this.add_websocket_handler(null,"localhost", protocols, get_ws);
    }
    
    void get_ws(Soup.Server server, Soup.WebsocketConnection connection, string path, Soup.ClientContext client){
        connection.message.connect(ws_message);
        string host = client.get_host();
        info (@"Client connected! Host: $host");
        string msg = """test message1""";
        info (@"Sending to client message: $msg");
        connection.send_text(msg);
        this.ws = connection;
    }

    void ws_message(int id, Bytes msg){
        string message = (string)msg.get_data();
        info(@"Message received! ID: $id Message:\n$message\n");
    }
}

int main (string[] args)
{
	try {
		int port = 8088;
		MainLoop loop = new MainLoop ();
		WSServer server = new WSServer ();
		server.listen_local (port, 0);
		loop.run ();
	} catch (Error e) {
		error ("Error: %s\n", e.message);
	}

	stdout.printf("Programm end. Press ENTER");
    stdin.read_line ();
	return 0;
}
