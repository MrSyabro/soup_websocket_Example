/* client.vala
 *
 * Copyright 2020 MrSyabro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

static Soup.WebsocketConnection websocket;

int main (string[] args)
{
    var ws_session = new Soup.Session();
    var ws_message = new Soup.Message("GET", "http://127.0.0.1:8088/"); // указываем сервер и порт
    
    var ws_loop = new MainLoop();
    ws_session.websocket_connect_async.begin(ws_message, null, null, null, (obj, res) => { // асинхронно создаем соедиинение с сервером
	    websocket = ws_session.websocket_connect_async.end(res);
	    websocket.message.connect(rec_message); // соединяем сигнальчики сокета
        websocket.closed.connect(ws_closed); // и туть
        websocket.error.connect(ws_error); // и туть тоже
    });
    ws_loop.run();

    stdout.printf("Program end. Press ENTER");
    stdin.read_line(); // тут все.. Для окончания всех асинхронных операций тут остановочка
    websocket.close(0, null);
    return 0;
}

void rec_message(int type, Bytes message){
    // Вот тут то обрабатываем полученные сообщения
    stdout.printf("WebSock receive:\n");
    stdout.printf("%s\n".printf((string)message.get_data()));
}

void ws_closed(){
    // Если закрылся
    stdout.printf("WebSock closed!");
}

void ws_error(Error error){
    // Вдруг ошибосик сокета
    int code = error.code;
    string message = error.message;
    stdout.printf("WS Error %d: %s\n".printf(code,message));
}
